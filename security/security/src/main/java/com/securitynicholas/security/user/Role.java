package com.securitynicholas.security.user;

public enum Role {
    USER,
    ADMIN
}
